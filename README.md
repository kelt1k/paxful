# Welcome 
Here is my test task 

# App
The main app is localted in /app directory

To work with application localy you need to:
## Quick Start

1. From app folder create virtual environment using python3 and activate it
```
$ virtualenv env
$ source env/bin/activate
```
2. Install dependencies in virtualenv
```
$ pip install -r requirements.txt
```
3. Export necessary env variables from .env file or directly from cli via 
``` 
export VARIABLE=value
``` 
Application waits for next vaiables:
  - db_password
  - db_username
  - db_host
  - GMAIL_PASS


4. Start application with
```
$ gunicorn --bind 0.0.0.0:8000 wsgi:app
```

Application need DB for correct work so you can use docker-compose file to start all stack at once
or comment out application to run only DB and adminer (web-based admin pannel for DB management) with:
```
docker-compose up -d 
``` 

# Deploy to k8s

## minikube prepare

For local deployment you can use minikube
Here is a link to the minikube installation [instructions](https://minikube.sigs.k8s.io/docs/start/)
Please note that app deployment needs ingress controller enabled. So after minikube up and running do not forget to enable nginx ingress addon
``` 
minikube addons enable ingress
``` 

## App deployment

1. For application deployment first create and deploy secret to default minukube namespace:
``` 
echo EOF<< | tee .env
db_username=postgres
GMAIL_PASS=google_token_for_you_app
db_host='db-pgpool'
>>

$ kubectl create secret generic paxful --from-env-file .env --dry-run -o yaml | kubectl apply -f -
```
2. Navigate to helm/ directory: 

``` 
$ cd helm/
``` 
3. Update chart dependencies:
``` 
$ helm dependency update paxful
``` 

4. Deploy chart:
``` 
$  helm upgrade -i paxful -f paxful/values.yaml paxful
``` 

5. Ensure all pods are up and running:

``` 
kubectl get pods
``` 

6. Check that ingress created and ready: 

```
kubectl get ing jce-webapp -o json | jq -r '.status.loadBalancer.ingress[0].ip'
```

7. Add recieved ip address to /etc/hosts

```
echo "$(kubectl get ing paxful -o json | jq -r '.status.loadBalancer.ingress[0].ip') | sudo tee -a /etc/hosts
``` 

8. Enable activate access to minikube's ingress
```
minikube tunnel
``` 
9. try to reach ingress via curl:
``` 
curl http://paxful.local/?n=5 (for the first case)
curl http://paxful.local/blacklisted (for the  second case)
curl http://paxful.local/blacklisted (to ensure that ip is)
``` 
