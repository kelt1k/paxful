FROM python:3.9
RUN mkdir /app 
COPY app /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "wsgi:app"]