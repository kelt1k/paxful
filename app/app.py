# from unittest import result
from flask import Flask, request, abort, jsonify
from flask_mail import Mail, Message
import werkzeug
from sqlalchemy import create_engine, MetaData, Table
import datetime
import os
from db import db_session, init_db
from models import User

app = Flask(__name__)

init_db()
mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": 'kelt.max@gmail.com',
    "MAIL_PASSWORD": os.getenv('GMAIL_PASS')
}

app.config.update(mail_settings)
mail = Mail(app)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

@app.errorhandler(werkzeug.exceptions.BadRequest)

@app.before_request
def block_method():
    ip = request.environ.get('REMOTE_ADDR')
    ip_ban_list = str(User.query.all())
    if ip in ip_ban_list:
        abort(403)

@app.route('/')
def data():
  n = request.args.get('n')
  return (str(int(n) * int(n)))

@app.route('/blacklisted')
def handle_bad_request():
    ip = request.environ['REMOTE_ADDR']
    path = request.environ['PATH_INFO']
    u = User(path, ip, datetime.datetime.now())
    db_session.add(u)
    db_session.commit()
    msg = Message(subject="Blocked IP", sender=app.config.get('MAIL_USERNAME'), recipients=["kelt.max@gmail.com"], body=(f'Blocked IP:  {ip}'))
    mail.send(msg)
    return (f'your IP: {ip}  was blacklisted') , 444


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)