from sqlalchemy import Column, Integer, String, Date, DateTime
from db import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    path = Column(String(50), unique=False)
    ip = Column(String(50), unique=True)
    date = Column(DateTime(timezone=False))
    # email = Column(String(120), unique=True)

    def __init__(self, path=None, ip=None, date=None):
        self.path = path
        self.ip = ip
        self.date = date

    def __repr__(self):
        return self.ip